import torch
from torch import nn
import torchvision
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor, Lambda
import numpy as np

torch.backends.cudnn.enabled = False

ntrain = 4000
ntest = 1000
T = 5

hidden_dim = np.array([2, 6, 10, 15, 20, 25, 30, 35, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 60, 70, 80, 100])
params = np.zeros(hidden_dim.shape)

training_data = datasets.MNIST(
    root="data",
    train=True,
    download=True,
    transform=ToTensor(),
    target_transform = Lambda(lambda y: torch.zeros(
        10, dtype=torch.float).scatter_(dim=0, index=torch.tensor(y), value=1))
)

test_data = datasets.MNIST(
    root="data",
    train=False,
    download=True,
    transform=ToTensor(),
    target_transform = Lambda(lambda y: torch.zeros(
        10, dtype=torch.float).scatter_(dim=0, index=torch.tensor(y), value=1)),
)
training_indices = torch.randperm(len(training_data))[:ntrain]
test_indices = torch.randperm(len(test_data))[:ntest]

training_data = torch.utils.data.Subset(training_data, training_indices)
test_data = torch.utils.data.Subset(test_data, test_indices)

train_dataloader = DataLoader(training_data, batch_size=32)
test_dataloader = DataLoader(test_data, batch_size=32)

class NeuralNetwork(nn.Module):
    def __init__(self, h_size):
        super(NeuralNetwork, self).__init__()
        self.flatten = nn.Flatten()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(28*28, h_size),
            nn.ReLU(),
            nn.Linear(h_size, 10),
        )

    def forward(self, x):
        x = self.flatten(x)
        logits = self.linear_relu_stack(x)
        return logits

learning_rate = 1e-3
batch_size_train = 32
epochs = 100
batch_size_test = ntest
random_seed = 2021
momentum = 0.95

train_error = np.zeros((hidden_dim.size, T))
test_error = np.zeros((hidden_dim.size, T))

train_accuracy = np.zeros((hidden_dim.size, T))
test_accuracy = np.zeros((hidden_dim.size, T))

def train_loop(dataloader, model, loss_fn, optimizer):
    model.train()
    size = len(dataloader.dataset)
    for batch, (X, y) in enumerate(dataloader):
        # Compute prediction and loss
        pred = model(X)
        loss = loss_fn(pred, y)

        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch % 100 == 0:
            loss, current = loss.item(), batch * len(X)
            print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")

    train_loss, correct = 0, 0
    with torch.no_grad():
        for X, y in dataloader:
            pred = model(X)
            train_loss += loss_fn(pred, y).item()
            pred_label = pred.data.max(1, keepdim=True)[1]
            correct += pred_label.eq(y.data.max(1, keepdim=True)[1]).sum().item()
    train_loss /= len(dataloader)
    correct /= len(dataloader.dataset)
    return train_loss, correct



def test_loop(dataloader, model, loss_fn):
    model.eval()
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    test_loss, correct = 0, 0

    with torch.no_grad():
        for X, y in dataloader:
            pred = model(X)
            test_loss += loss_fn(pred, y).item()
            pred_label = pred.data.max(1, keepdim=True)[1]
            correct += pred_label.eq(y.data.max(1, keepdim=True)[1]).sum().item()

    test_loss /= num_batches
    print(correct)
    print(size)
    correct /= size
    print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} \n")
    return test_loss, correct

params = np.zeros(hidden_dim.size)

for t in range(T):
    for i, h in enumerate(hidden_dim):
        model = NeuralNetwork(h)
        model_parameters = filter(lambda p: p.requires_grad, model.parameters())
        params[i] = sum([np.prod(p.size()) for p in model_parameters])
        loss_fn = nn.MSELoss()
        optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate, momentum=momentum)
        for j in range(epochs):
            print(f"Epoch {j+1}\n-------------------------------")
            _, _ = train_loop(train_dataloader, model, loss_fn, optimizer)
            _, _ = test_loop(test_dataloader, model, loss_fn)
        print("Done!")
        train_error[i, t], train_accuracy[i, t] = train_loop(train_dataloader, model, loss_fn, optimizer)
        test_error[i, t], test_accuracy[i, t] = test_loop(test_dataloader, model, loss_fn)
        
train_error_mean = np.mean(train_error, axis=1)
test_error_mean = np.mean(test_error, axis=1)
train_accuracy_mean = np.mean(train_accuracy, axis=1)
test_accuracy_mean = np.mean(test_accuracy, axis=1)

plt.plot(params / ntrain, train_error_mean, label = 'train MSE')
plt.plot(params / ntrain, test_error_mean, label = 'test MSE')
plt.legend()
plt.show()

print(train_error_mean)
print(test_error_mean)

plt.plot(params / ntrain, 1 - train_accuracy_mean, label = 'train err')
plt.plot(params / ntrain, 1 - test_accuracy_mean, label = 'test err')
plt.legend()
plt.show()