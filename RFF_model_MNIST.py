import tensorflow as tf
import numpy as np
import scipy
from sklearn.utils import shuffle
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import OneHotEncoder
from sklearn.linear_model import Ridge
from sklearn.metrics import mean_squared_error
from matplotlib import pyplot as plt



(train_images, train_labels), (test_images, test_labels) = tf.keras.datasets.mnist.load_data()

train_images = train_images.reshape(train_images.shape[0], 28*28).astype('float32')
test_images = test_images.reshape(test_images.shape[0], 28*28).astype('float32')

train_images = train_images / 255.0
test_images = test_images / 255.0

ntrain = 4000
ntest = 1000
l = 1e-3

train_images, train_labels = shuffle(train_images, train_labels)
test_images, test_labels = shuffle(test_images, test_labels)
train_images = train_images[0:ntrain, :]
train_labels = train_labels[0:ntrain]
test_images = test_images[0:ntest, :]
test_labels = test_labels[0:ntest]

train_labels_pure = train_labels
test_labels_pure = test_labels
one_enc = OneHotEncoder(sparse=False)
train_labels = train_labels.reshape((train_labels.size, 1))
test_labels = test_labels.reshape((test_labels.size, 1))
one_enc.fit(train_labels)
train_labels = one_enc.transform(train_labels).astype('float32')
test_labels = one_enc.transform(test_labels).astype('float32')


d = train_images.shape[1]

Ns = (np.arange(50) + 0.1) * ntrain / 10
T = 5
l = 1e-3

mu_0 = 1. / np.sqrt(2*np.pi)
mu_1 = 1. / 2.
mu_s = np.sqrt(1./2. - mu_0**2 - mu_1**2)


Ns = Ns.astype(np.int64)

print(train_images.shape)

def sample_spherical(npoints, ndim):
    vec = np.random.randn(npoints, ndim)
    vec = vec / np.linalg.norm(vec, axis=1)[:, np.newaxis]
    return vec * np.sqrt(ndim)

def sample_theta(outerdim, innerdim):
    theta = np.random.randn(outerdim, innerdim)
    theta = theta / np.linalg.norm(theta, axis=1)[:, np.newaxis]
    return theta * np.sqrt(innerdim)

def ReLU(x):
    return x * (x > 0)

test_error_matrix = np.zeros((Ns.size, T))
train_error_matrix = np.zeros((Ns.size, T))
train_accuracy_matrix = np.zeros((Ns.size, T))
test_accuracy_matrix = np.zeros((Ns.size, T))
weight_matrix = np.zeros((Ns.size, T))

for i, N in enumerate(Ns):
    print(N)
    for t in np.arange(T):
        print(t)
        xtrain = train_images
        xtest = test_images

        ytrain = train_labels
        ytest = test_labels
        ytrain_pure = train_labels_pure
        ytest_pure = test_labels_pure

        theta = sample_theta(N, d)
        Xtrain = xtrain @ theta.T / np.sqrt(d)
        Xtrain = ReLU(Xtrain)

        Xtest = xtest @ theta.T / np.sqrt(d)
        Xtest = ReLU(Xtest)

        model = Ridge(alpha = l*N/d, tol = 1e-3)
        model.fit(Xtrain, ytrain)
        weight_matrix[i, t] = mu_s**2*N*np.linalg.norm(model.coef_)/ntrain


        ytest_pred = model.predict(Xtest)
        ytrain_pred = model.predict(Xtrain)

        ytrain_pred_clean = np.zeros((ntrain, 10))
        ytest_pred_clean = np.zeros((ntest, 10))
        ytrain_pred_clean[np.arange(ntrain), np.argmax(ytrain_pred, axis=1)] = 1
        ytest_pred_clean[np.arange(ntest), np.argmax(ytest_pred, axis=1)] = 1
        ytrain_pred_clean = one_enc.inverse_transform(ytrain_pred_clean).reshape((ntrain))
        ytest_pred_clean = one_enc.inverse_transform(ytest_pred_clean).reshape((ntest))

        train_accuracy_matrix[i, t] = accuracy_score(ytrain_pure, ytrain_pred_clean)
        test_accuracy_matrix[i, t] = accuracy_score(ytest_pure, ytest_pred_clean)

        train_error = mean_squared_error(ytrain, ytrain_pred)
        test_error = mean_squared_error(ytest, ytest_pred)

        train_error_matrix[i, t] = train_error
        test_error_matrix[i, t] = test_error

train_error_mean = np.mean(train_error_matrix, axis=1)
test_error_mean = np.mean(test_error_matrix, axis=1)
weight_mean = np.mean(weight_matrix, axis=1)
train_error_std = np.std(train_error_matrix, axis=1)
test_error_std = np.std(test_error_matrix, axis=1)
weight_std = np.std(weight_matrix, axis=1)

train_accuracy_mean = np.mean(train_accuracy_matrix, axis=1)
test_accuracy_mean = np.mean(test_accuracy_matrix, axis=1)

plt.errorbar(Ns/ntrain, train_error_mean, train_error_std, label='train MSE')
plt.errorbar(Ns/ntrain, test_error_mean, test_error_std, label='test MSE')
#plt.errorbar(Ns/ntrain, weight_mean, weight_std, label='weights')
plt.xlabel('N/n')
plt.ylabel('Error')
plt.title('RF model on MNIST')
plt.legend()
plt.show()

plt.plot(Ns/ntrain, train_accuracy_mean, label='train acc')
plt.plot(Ns/ntrain, test_accuracy_mean, label='test acc')
plt.xlabel('N/n')
plt.ylabel('Accuracy')
plt.title('RF model on MNIST')
plt.legend()
plt.show()

print("End")


