import numpy as np
import scipy
from sklearn.linear_model import Ridge
from sklearn.metrics import mean_squared_error
from matplotlib import pyplot as plt

np.random.seed(1)

D = 100
P = 1000
SNR = 0.0025
Ntest = 10000
Ntrains = 10**(np.arange(start=-1, stop=2, step=0.1))*D
print(Ntrains)
gamma = 1e-3
T = 3

mu_0 = 1. / np.sqrt(2*np.pi)
mu_1 = 1. / 2.
mu_s = np.sqrt(1./2. - mu_0**2 - mu_1**2)



def sample_theta(params, dim):
    theta = np.random.randn(params, dim)
    return theta

def sample_X(ntrain, dim):
    X = np.random.randn(ntrain, dim)
    return X

def sample_beta(dim):
    beta = np.random.randn(dim)
    return beta

def identity(x):
    return x

def ReLU(x):
    return x * (x > 0)

def sigmoid(x):
    return 1/(1 + np.exp(-x))

def tanh(x):
    return np.tanh(x)

beta = sample_beta(D)

train_error_id = np.zeros((Ntrains.size, T))
test_error_id = np.zeros((Ntrains.size, T))

train_error_relu = np.zeros((Ntrains.size, T))
test_error_relu = np.zeros((Ntrains.size, T))

train_error_sig = np.zeros((Ntrains.size, T))
test_error_sig = np.zeros((Ntrains.size, T))

train_error_abs = np.zeros((Ntrains.size, T))
test_error_abs = np.zeros((Ntrains.size, T))

train_error_tanh = np.zeros((Ntrains.size, T))
test_error_tanh = np.zeros((Ntrains.size, T))


for t in range(T):
    beta = sample_beta(D)
    for i, Ntrain in enumerate(Ntrains):
        Ntrain = np.floor(Ntrain).astype(np.int32)
        Xtrain = sample_X(Ntrain, D)
        Xtest = sample_X(Ntest, D)
        print(Xtrain.shape)
        print(beta.shape)
        Ytrain = Xtrain @ beta.T
        Ytrain = Ytrain + np.random.randn(Ntrain)/np.sqrt(SNR)
        Ytest = Xtest @ beta.T
        Ytest = Ytest + np.random.randn(Ntest)/np.sqrt(SNR)
        theta = sample_theta(P, D)

        # ReLU
        Ztrain = Xtrain @ theta.T / np.sqrt(D)
        Ztrain = ReLU(Ztrain)

        Ztest = Xtest @ theta.T / np.sqrt(D)
        Ztest = ReLU(Ztest)

        model = Ridge(alpha = gamma*P/D, tol = 1e-10)
        model.fit(Ztrain, Ytrain)

        ytest_pred = model.predict(Ztest)
        ytrain_pred = model.predict(Ztrain)

        train_error = mean_squared_error(Ytrain, ytrain_pred)
        test_error = mean_squared_error(Ytest, ytest_pred)

        train_error_relu[i, t] = train_error
        test_error_relu[i, t] = test_error

        # Identity
        Ztrain = Xtrain @ theta.T / np.sqrt(D)
        Ztrain = identity(Ztrain)

        Ztest = Xtest @ theta.T / np.sqrt(D)
        Ztest = identity(Ztest)

        model = Ridge(alpha = gamma*P/D, tol = 1e-10)
        model.fit(Ztrain, Ytrain)

        ytest_pred = model.predict(Ztest)
        ytrain_pred = model.predict(Ztrain)

        train_error = mean_squared_error(Ytrain, ytrain_pred)
        test_error = mean_squared_error(Ytest, ytest_pred)

        train_error_id[i, t] = train_error
        test_error_id[i, t] = test_error

        # Sigmoid

        Ztrain = Xtrain @ theta.T / np.sqrt(D)
        Ztrain = sigmoid(Ztrain)

        Ztest = Xtest @ theta.T / np.sqrt(D)
        Ztest = sigmoid(Ztest)

        model = Ridge(alpha = gamma*P/D, tol = 1e-10)
        model.fit(Ztrain, Ytrain)

        ytest_pred = model.predict(Ztest)
        ytrain_pred = model.predict(Ztrain)

        train_error = mean_squared_error(Ytrain, ytrain_pred)
        test_error = mean_squared_error(Ytest, ytest_pred)

        train_error_sig[i, t] = train_error
        test_error_sig[i, t] = test_error

        # Tanh
        Ztrain = Xtrain @ theta.T / np.sqrt(D)
        Ztrain = tanh(Ztrain)

        Ztest = Xtest @ theta.T / np.sqrt(D)
        Ztest = tanh(Ztest)

        model = Ridge(alpha = gamma*P/D, tol = 1e-10)
        model.fit(Ztrain, Ytrain)

        ytest_pred = model.predict(Ztest)
        ytrain_pred = model.predict(Ztrain)

        train_error = mean_squared_error(Ytrain, ytrain_pred)
        test_error = mean_squared_error(Ytest, ytest_pred)

        train_error_tanh[i, t] = train_error
        test_error_tanh[i, t] = test_error

        # Abs
        Ztrain = Xtrain @ theta.T / np.sqrt(D)
        Ztrain = abs(Ztrain)

        Ztest = Xtest @ theta.T / np.sqrt(D)
        Ztest = abs(Ztest)

        model = Ridge(alpha = gamma*P/D, tol = 1e-10)
        model.fit(Ztrain, Ytrain)

        ytest_pred = model.predict(Ztest)
        ytrain_pred = model.predict(Ztrain)

        train_error = mean_squared_error(Ytrain, ytrain_pred)
        test_error = mean_squared_error(Ytest, ytest_pred)

        train_error_abs[i, t] = train_error
        test_error_abs[i, t] = test_error



plt.figure()
plt.plot(Ntrains/D, np.mean(train_error_relu, axis=1), label='train MSE')
plt.plot(Ntrains/D, np.mean(test_error_relu, axis=1), label='test MSE')
plt.title('ReLU')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('N/D')
plt.ylabel('Error')
plt.legend()
plt.show()

plt.figure()
plt.plot(Ntrains/D, np.mean(train_error_sig, axis=1), label='train MSE')
plt.plot(Ntrains/D, np.mean(test_error_sig, axis=1), label='test MSE')
plt.title('Sigmoid')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('N/D')
plt.ylabel('Error')
plt.legend()
plt.show()

plt.figure()
plt.plot(Ntrains/D, np.mean(train_error_abs, axis=1), label='train MSE')
plt.plot(Ntrains/D, np.mean(test_error_abs, axis=1), label='test MSE')
plt.title('Abs')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('N/D')
plt.ylabel('Error')
plt.legend()
plt.show()

plt.figure()
plt.plot(Ntrains/D, np.mean(train_error_id, axis=1), label='train MSE')
plt.plot(Ntrains/D, np.mean(test_error_id, axis=1), label='test MSE')
plt.title('Identity - Linear')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('N/D')
plt.ylabel('Error')
plt.legend()
plt.show()

plt.figure()
plt.plot(Ntrains/D, np.mean(train_error_tanh, axis=1), label='train MSE')
plt.plot(Ntrains/D, np.mean(test_error_tanh, axis=1), label='test MSE')
plt.title('Tanh')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('N/D')
plt.ylabel('Error')
plt.legend()
plt.show()

plt.figure()
plt.plot(Ntrains/D, np.mean(test_error_relu, axis=1), label='ReLU')
plt.plot(Ntrains/D, np.mean(test_error_sig, axis=1), label='Sigmoid')
plt.plot(Ntrains/D, np.mean(test_error_id, axis=1), label='Identity')
plt.plot(Ntrains/D, np.mean(test_error_abs, axis=1), label='Abs')
plt.plot(Ntrains/D, np.mean(test_error_tanh, axis=1), label='Tanh')
plt.title('Test errors')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('N/D')
plt.ylabel('Test Error')
plt.legend()
plt.show()

