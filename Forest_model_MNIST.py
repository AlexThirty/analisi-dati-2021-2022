import tensorflow as tf
import numpy as np
import scipy
from matplotlib import pyplot as plt
from sklearn.utils import shuffle
from sklearn.ensemble import RandomForestRegressor as RF
from sklearn.metrics import accuracy_score
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import OneHotEncoder

(train_images, train_labels), (test_images, test_labels) = tf.keras.datasets.mnist.load_data()

train_images = train_images.reshape(train_images.shape[0], 28*28).astype('float32')
test_images = test_images.reshape(test_images.shape[0], 28*28).astype('float32')

train_images = train_images / 255.0
test_images = test_images / 255.0

ntrain = 4000
ntest = 1000
l = 1e-3
T = 5

#incorrect_pos = np.random.binomial(1, 0.3, train_images.shape[0])
#incorrect_digit = np.random.randint(0, 10, train_images.shape[0])
#train_labels = train_labels * (1 - incorrect_pos) + incorrect_pos * incorrect_digit

train_labels_pure = train_labels
test_labels_pure = test_labels
one_enc = OneHotEncoder(sparse=False)
train_labels = train_labels.reshape((train_labels.size, 1))
test_labels = test_labels.reshape((test_labels.size, 1))
one_enc.fit(train_labels)
train_labels = one_enc.transform(train_labels).astype('float32')
test_labels = one_enc.transform(test_labels).astype('float32')
print(train_labels)
print(train_labels_pure)

#train_images, train_labels = shuffle(train_images, train_labels)
#test_images, test_labels = shuffle(test_images, test_labels)
xtrain = train_images[0:ntrain, :]
ytrain = train_labels[0:ntrain, :]
ytrain_pure = train_labels_pure[0:ntrain]
xtest = test_images[0:ntest, :]
ytest = test_labels[0:ntest, :]
ytest_pure = test_labels_pure[0:ntest]

leaves = np.array([10, 30, 50, 100, 250, 500, 750, 1000, 1250, 1500, 1750, 2000, 3000, 4000, 5000, 7000, 10000])
trees = np.array([1, 3, 5, 7, 9, 11, 13, 15, 20])
trees_leaves = np.array([[10,1], [50,1], [100,1], [200,1], [400,1], [600,1], [800,1], [1000,1], [1200,1], [1400,1],
                         [1600,1], [1800,1], [2000,1], [2000,2], [2000, 5], [2000,7], [2000,10], [2000, 15], [2000,20]])

#train_error = np.zeros((leaves.size, trees.size, T))
#test_error = np.zeros((leaves.size, trees.size, T))

#train_accuracy = np.zeros((leaves.size, trees.size, T))
#test_accuracy = np.zeros((leaves.size, trees.size, T))


#for t in range(T):
#    for i, tr in enumerate(trees):
#        for j, le in enumerate(leaves):
#            print(i)
#            print(j)
#            # Define the model and fit
#            RFclass = RF(tr, max_leaf_nodes=le, bootstrap=False, n_jobs=-1, max_features='sqrt')
#            RFclass.fit(xtrain, ytrain)

#            # Prediction
#            ytrain_pred = RFclass.predict(xtrain)
#            ytest_pred = RFclass.predict(xtest)

#            # Errors and label conversion
#            train_error[j, i, t] = mean_squared_error(ytrain, ytrain_pred)
#            test_error[j, i, t] = mean_squared_error(ytest, ytest_pred)
#            ytrain_pred_clean = np.zeros((ntrain, 10))
#            ytest_pred_clean = np.zeros((ntest, 10))
#            ytrain_pred_clean[np.arange(ntrain), np.argmax(ytrain_pred, axis=1)] = 1
#            ytest_pred_clean[np.arange(ntest), np.argmax(ytest_pred, axis=1)] = 1
#            ytrain_pred_clean = one_enc.inverse_transform(ytrain_pred_clean).reshape((ntrain))
#            ytest_pred_clean = one_enc.inverse_transform(ytest_pred_clean).reshape((ntest))
#            train_accuracy[j, i, t] = accuracy_score(ytrain_pure, ytrain_pred_clean)
#            test_accuracy[j, i, t] = accuracy_score(ytest_pure, ytest_pred_clean)

#train_accuracy_mean = np.mean(train_accuracy, axis=2)
#test_accuracy_mean = np.mean(test_accuracy, axis=2)

#train_error_mean = np.mean(train_error, axis=2)
#test_error_mean = np.mean(test_error, axis=2)

#ax1, ax2 = np.meshgrid(range(leaves.size), trees)
#print(ax1.shape)
#print(ax2.shape)
#print(train_error_mean.shape)
#fig = plt.figure()
#ax = fig.add_subplot(projection='3d')
#ax.plot_surface(ax1, ax2, train_error_mean.T)
#ax.plot_surface(ax1, ax2, test_error_mean.T)
#plt.show()

#fig = plt.figure()
#ax = fig.add_subplot(projection='3d')
#ax.plot_surface(ax1, ax2, train_accuracy_mean.T)
#ax.plot_surface(ax1, ax2, test_accuracy_mean.T)
#plt.show()

train_error = np.zeros((trees_leaves.shape[0], T))
test_error = np.zeros((trees_leaves.shape[0], T))

train_accuracy = np.zeros((trees_leaves.shape[0], T))
test_accuracy = np.zeros((trees_leaves.shape[0], T))

for t in range(T):
    for i, params in enumerate(trees_leaves):
        tr = params[1]
        le = params[0]

        # Define the model and fit
        RFregr = RF(tr, bootstrap=False, max_leaf_nodes=le, max_features='sqrt', n_jobs=-1)
        RFregr.fit(xtrain, ytrain)

        # Prediction
        ytrain_pred = RFregr.predict(xtrain)
        ytest_pred = RFregr.predict(xtest)

        # Errors and label cleaning
        train_error[i, t] = mean_squared_error(ytrain, ytrain_pred)
        test_error[i, t] = mean_squared_error(ytest, ytest_pred)
        ytrain_pred_clean = np.zeros((ntrain, 10))
        ytest_pred_clean = np.zeros((ntest, 10))
        ytrain_pred_clean[np.arange(ntrain), np.argmax(ytrain_pred, axis=1)] = 1
        ytest_pred_clean[np.arange(ntest), np.argmax(ytest_pred, axis=1)] = 1
        ytrain_pred_clean = one_enc.inverse_transform(ytrain_pred_clean).reshape((ntrain))
        ytest_pred_clean = one_enc.inverse_transform(ytest_pred_clean).reshape((ntest))
        train_accuracy[i, t] = accuracy_score(ytrain_pure, ytrain_pred_clean)
        test_accuracy[i, t] = accuracy_score(ytest_pure, ytest_pred_clean)

# Plotting
train_error_mean = np.mean(train_error, axis=1)
test_error_mean = np.mean(test_error, axis=1)

train_accuracy_mean = np.mean(train_accuracy, axis=1)
test_accuracy_mean = np.mean(test_accuracy, axis=1)

plt.plot(range(trees_leaves.shape[0]), train_error_mean, label='train MSE')
plt.plot(range(trees_leaves.shape[0]), test_error_mean, label='test MSE')
plt.legend()
plt.show()

plt.plot(range(trees_leaves.shape[0]), train_accuracy_mean, label='train ACC')
plt.plot(range(trees_leaves.shape[0]), test_accuracy_mean, label='test ACC')
plt.legend()
plt.show()