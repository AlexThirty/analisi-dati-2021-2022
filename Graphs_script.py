import numpy as np
from matplotlib import pyplot as plt
from sympy import nsolve, I, Symbol, sqrt

def sample_spherical(npoints, ndim):
    vec = np.random.randn(npoints, ndim)
    vec = vec / np.linalg.norm(vec, axis=1)[:, np.newaxis]
    return vec * np.sqrt(ndim)

def sample_theta(outerdim, innerdim):
    theta = np.random.randn(outerdim, innerdim)
    theta = theta / np.linalg.norm(theta, axis=1)[:, np.newaxis]
    return theta * np.sqrt(innerdim)

def f1(x, noise=True, sigma=np.sqrt(0.5)):
    vec = x[:,0]
    if noise is True:
        vec = vec + sigma*np.random.randn(vec.size)
    return vec

def f2(x, noise=False, sigma=np.sqrt(0.5)):
    vec = x[:,0] + (x[:,0]**2 - 1)/2
    if noise is True:
        vec = vec + sigma*np.random.randn(vec.size)
    return vec

def f3(x, noise=False, sigma=np.sqrt(0.5)):
    vec = x[:,0] + x[:,0]*x[:,1]/np.sqrt(2)
    if noise is True:
        vec = vec + sigma*np.random.randn(vec.size)
    return vec

def ReLU(x):
    return x * (x > 0)

mu_0 = 1. / np.sqrt(2*np.pi)
mu_1 = 1. / 2.
mu_s = np.sqrt(1./2. - mu_0**2 - mu_1**2)
print(mu_s)
zeta = mu_1 / mu_s

def ridgeless_calc(F0, F1, Fs, tau, psi_2, psi_1):
    psi = np.min(np.array([psi_1, psi_2]))
    chi = -(np.sqrt((psi*zeta**2 - zeta**2 - 1)**2 + 4*zeta**2*psi) + (psi*zeta**2 - zeta**2 - 1))/(2*zeta**2)
    eps_0 = -chi**5*zeta**6 + 3*chi**4*zeta**4 + (psi_1*psi_2 - psi_2 - psi_1 + 1)*chi**3*zeta**6 - 2*chi**3*zeta**4 - 2*chi**3*zeta**4 - 3*chi**3*zeta**2 + (psi_1 + psi_2 - 3*psi_1*psi_2 + 1)*chi**2*zeta**4 + 2*chi**2*zeta**2 + chi**2 + 3*psi_1*psi_2*chi*zeta**2 - psi_1*psi_2
    eps_1 = psi_2*chi**3*zeta**4 - psi_2*zeta**2*chi**2 + psi_1*psi_2*chi*zeta**2 - psi_1*psi_2
    eps_2 = chi**5*zeta**6 - 3*chi**4*zeta**4 + (psi_1-1)*chi**3*zeta**6 + 2*chi**3*zeta**4 + 3*chi**3*zeta**2 + (-psi_1-1)*zeta**4*chi**2 - 2*chi**2*zeta**2 - chi**2
    eps_0 = abs(eps_0)
    eps_1 = abs(eps_1)
    eps_2 = abs(eps_2)
    B = eps_1/eps_0
    V = eps_2/eps_0
    return B, V, F1**2*B+(tau**2+Fs**2)*V+Fs**2

F0 = 0.
F1 = 1.
Fs = 0.
tau = 1.
psi_2 = 2.

psi_1s = np.arange(0, 3, 0.05) * psi_2
Rs = np.zeros(psi_1s.size)
Bs = np.zeros(psi_1s.size)
Vs = np.zeros(psi_1s.size)


for i, psi_1 in enumerate(psi_1s):
    Bs[i], Vs[i], Rs[i] = ridgeless_calc(F0, F1, Fs, tau, psi_2, psi_1)

plt.figure()
plt.plot(psi_1s/psi_2, Bs, label='Bias')
plt.plot(psi_1s/psi_2, Vs, label='Variance')
plt.plot(psi_1s/psi_2, Rs, label='Risk')
plt.xlabel('N/n = psi_1/psi_2')
plt.ylabel('Test error')
plt.legend()
plt.show()
