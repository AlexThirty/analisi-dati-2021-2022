import tensorflow as tf
import tensorflow_datasets as tfds
import numpy as np
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt

data = tfds.load('wine_quality', split='train', batch_size=-1, as_supervised=True)

xs, ys = tfds.as_numpy(data)

ys = ys.astype('float32')
ys = ys + np.random.randn(ys.size)
data_array = np.empty([xs['alcohol'].size, len(xs)])
for i, name in enumerate(xs):
    data_array[:, i] = xs[name]

ntrain = 4000
ntest = data_array.shape[0] - ntrain
T = 2
l = 1e-3
hidden_dim = np.array([10, 20, 50, 100, 120, 150, 200, 300, 400, 500])

train_error = np.zeros((hidden_dim.size, T))
test_error = np.zeros((hidden_dim.size, T))

params = np.zeros(hidden_dim.size)

for t in range(T):
    data_array, ys = shuffle(data_array, ys)
    xtrain, xtest, ytrain, ytest = train_test_split(data_array, ys, train_size=ntrain)

    for i, H in enumerate(hidden_dim):
        model = tf.keras.models.Sequential([
            tf.keras.layers.InputLayer(input_shape=data_array.shape[1]),
            tf.keras.layers.Dense(H, activation = 'relu'),
            tf.keras.layers.Dense(20, activation = 'relu'),
            tf.keras.layers.Dense(1)
        ])

        model.compile(optimizer='adam',
                        loss = tf.keras.losses.MeanSquaredError(),
                        metrics = [tf.keras.metrics.MeanSquaredError()]
        )

        model.summary()
        trainableParams = np.sum([np.prod(v.get_shape()) for v in model.trainable_weights])
        params[i] = trainableParams
    
        model.fit(xtrain, ytrain, epochs=10)

        train_loss, _ = model.evaluate(xtrain, ytrain, verbose = 2)
        test_loss, _ = model.evaluate(xtest, ytest, verbose = 2)

        train_error[i, t] = train_loss
        test_error[i, t] = test_loss

train_error_mean = np.mean(train_error, axis=1)
test_error_mean = np.mean(test_error, axis=1)

plt.plot(params / ntrain, train_error_mean, label = 'train MSE')
plt.plot(params / ntrain, test_error_mean, label = 'test MSE')
plt.legend()
plt.show()



