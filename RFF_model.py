import numpy as np
import scipy
from sklearn.linear_model import Ridge
from sklearn.metrics import mean_squared_error
from matplotlib import pyplot as plt

d = 100
ntrain = 300
ntest = 100
Ns = (np.arange(50) + 0.1) * ntrain / 10
T = 20
l = 1e-3

mu_0 = 1. / np.sqrt(2*np.pi)
mu_1 = 1. / 2.
mu_s = np.sqrt(1./2. - mu_0**2 - mu_1**2)

Ns = Ns.astype(np.int64)

def sample_spherical(npoints, ndim):
    vec = np.random.randn(npoints, ndim)
    vec = vec / np.linalg.norm(vec, axis=1)[:, np.newaxis]
    return vec * np.sqrt(ndim)

def sample_theta(outerdim, innerdim):
    theta = np.random.randn(outerdim, innerdim)
    theta = theta / np.linalg.norm(theta, axis=1)[:, np.newaxis]
    return theta * np.sqrt(innerdim)

def f1(x, noise=True, sigma=np.sqrt(0.5)):
    vec = x[:,0]
    if noise is True:
        vec = vec + sigma*np.random.randn(vec.size)
    return vec

def f2(x, noise=False, sigma=np.sqrt(0.5)):
    vec = x[:,0] + (x[:,0]**2 - 1)/2
    if noise is True:
        vec = vec + sigma*np.random.randn(vec.size)
    return vec

def f3(x, noise=False, sigma=np.sqrt(0.5)):
    vec = x[:,0] + x[:,0]*x[:,1]*4
    if noise is True:
        vec = vec + sigma*np.random.randn(vec.size)
    return vec

def ReLU(x):
    return x * (x > 0)

test_error_matrix_1 = np.zeros((Ns.size, T))
train_error_matrix_1 = np.zeros((Ns.size, T))
weights_matrix_1 = np.zeros((Ns.size, T))

data_train = np.zeros((ntrain, d, T))
data_test = np.zeros((ntest, d, T))
for i in range(T):
    data_train[:,:,i] = sample_spherical(ntrain, d)
    data_test[:,:,i] = sample_spherical(ntest, d)

for i, N in enumerate(Ns):
    for t in np.arange(T):
        xtrain = data_train[:,:,t].reshape((ntrain, d))
        xtest = data_test[:,:,t].reshape((ntest, d))
        ytrain = f1(xtrain)
        ytest = f1(xtest)
        theta = sample_theta(N, d)

        Xtrain = xtrain @ theta.T / np.sqrt(d)
        Xtrain = ReLU(Xtrain)

        Xtest = xtest @ theta.T / np.sqrt(d)
        Xtest = ReLU(Xtest)

        model = Ridge(alpha = l*N/d, tol = 1e-10)
        model.fit(Xtrain, ytrain)
        weights_matrix_1[i, t] = mu_s**2*N*np.linalg.norm(model.coef_)/ntrain

        ytest_pred = model.predict(Xtest)
        ytrain_pred = model.predict(Xtrain)

        train_error = mean_squared_error(ytrain, ytrain_pred)
        test_error = mean_squared_error(ytest, ytest_pred)

        train_error_matrix_1[i, t] = train_error
        test_error_matrix_1[i, t] = test_error

train_error_mean_1 = np.mean(train_error_matrix_1, axis=1)
test_error_mean_1 = np.mean(test_error_matrix_1, axis=1)
weights_mean_1 = np.mean(weights_matrix_1, axis=1)
train_error_std_1 = np.std(train_error_matrix_1, axis=1)
test_error_std_1 = np.std(test_error_matrix_1, axis=1)

#plt.errorbar(Ns/ntrain, train_error_mean_1, train_error_std_1, label='train MSE')
#plt.errorbar(Ns/ntrain, test_error_mean_1, test_error_std_1, label='test MSE')

test_error_matrix_2 = np.zeros((Ns.size, T))
train_error_matrix_2 = np.zeros((Ns.size, T))
weights_matrix_2 = np.zeros((Ns.size, T))

for i, N in enumerate(Ns):
    for t in np.arange(T):
        xtrain = data_train[:,:,t].reshape((ntrain, d))
        xtest = data_test[:,:,t].reshape((ntest, d))
        ytrain = f2(xtrain)
        ytest = f2(xtest)
        
        theta = sample_theta(N, d)

        Xtrain = xtrain @ theta.T / np.sqrt(d)
        Xtrain = ReLU(Xtrain)

        Xtest = xtest @ theta.T / np.sqrt(d)
        Xtest = ReLU(Xtest)

        model = Ridge(alpha = l*N/d)
        model.fit(Xtrain, ytrain)
        weights_matrix_2[i, t] = mu_s**2*N*np.linalg.norm(model.coef_)/ntrain

        ytest_pred = model.predict(Xtest)
        ytrain_pred = model.predict(Xtrain)

        train_error = mean_squared_error(ytrain, ytrain_pred)
        test_error = mean_squared_error(ytest, ytest_pred)

        train_error_matrix_2[i, t] = train_error
        test_error_matrix_2[i, t] = test_error

train_error_mean_2 = np.mean(train_error_matrix_2, axis=1)
test_error_mean_2 = np.mean(test_error_matrix_2, axis=1)
weights_mean_2 = np.mean(weights_matrix_2, axis=1)
train_error_std_2 = np.std(train_error_matrix_2, axis=1)
test_error_std_2 = np.std(test_error_matrix_2, axis=1)

#plt.errorbar(Ns/ntrain, train_error_mean_2, train_error_std_2, label='train MSE')
#plt.errorbar(Ns/ntrain, test_error_mean_2, test_error_std_2, label='test MSE')


test_error_matrix_3 = np.zeros((Ns.size, T))
train_error_matrix_3 = np.zeros((Ns.size, T))
weights_matrix_3 = np.zeros((Ns.size, T))

for i, N in enumerate(Ns):
    for t in np.arange(T):
        xtrain = data_train[:,:,t].reshape((ntrain, d))
        xtest = data_test[:,:,t].reshape((ntest, d))
        ytrain = f3(xtrain)
        ytest = f3(xtest)
        
        theta = sample_theta(N, d)

        Xtrain = xtrain @ theta.T / np.sqrt(d)
        Xtrain = ReLU(Xtrain)

        Xtest = xtest @ theta.T / np.sqrt(d)
        Xtest = ReLU(Xtest)

        model = Ridge(alpha = l*N/d, tol = 1e-10)
        model.fit(Xtrain, ytrain)
        weights_matrix_3[i, t] = mu_s**2*N*np.linalg.norm(model.coef_)/ntrain

        ytest_pred = model.predict(Xtest)
        ytrain_pred = model.predict(Xtrain)

        train_error = mean_squared_error(ytrain, ytrain_pred)
        test_error = mean_squared_error(ytest, ytest_pred)

        train_error_matrix_3[i, t] = train_error
        test_error_matrix_3[i, t] = test_error

train_error_mean_3 = np.mean(train_error_matrix_3, axis=1)
test_error_mean_3 = np.mean(test_error_matrix_3, axis=1)
weights_mean_3 = np.mean(weights_matrix_3, axis=1)
train_error_std_3 = np.std(train_error_matrix_3, axis=1)
test_error_std_3 = np.std(test_error_matrix_3, axis=1)

max_error_1 = np.max(test_error_mean_1)
max_error_2 = np.max(test_error_mean_2)
max_error_3 = np.max(test_error_mean_3)
max_error = np.max(np.array([max_error_1, max_error_2, max_error_3]))

plt.plot(Ns/ntrain, train_error_mean_1, label='train MSE')
plt.plot(Ns/ntrain, test_error_mean_1, label='test MSE')
plt.plot(Ns/ntrain, weights_mean_1, label='weights')
plt.ylim(0, 5)
plt.title('First setting')
plt.ylabel('Error')
plt.xlabel('N/n')
plt.legend()
plt.show()

plt.plot(Ns/ntrain, test_error_mean_1, label='test MSE')
plt.plot(Ns/ntrain, weights_mean_1, label='weights')
plt.title('First setting')
plt.yscale('log')
plt.ylabel('Error')
plt.xlabel('N/n')
plt.legend()
plt.show()

plt.plot(Ns/ntrain, train_error_mean_2, label='train MSE')
plt.plot(Ns/ntrain, test_error_mean_2, label='test MSE')
plt.plot(Ns/ntrain, weights_mean_2, label='weights')
plt.ylim(0, 5)
plt.title('Second setting')
plt.ylabel('Error')
plt.xlabel('N/n')
plt.legend()
plt.show()

plt.plot(Ns/ntrain, test_error_mean_2, label='test MSE')
plt.plot(Ns/ntrain, weights_mean_2, label='weights')
plt.title('Second setting')
plt.ylabel('Error')
plt.yscale('log')
plt.xlabel('N/n')
plt.legend()
plt.show()

#plt.errorbar(Ns/ntrain, train_error_mean_3, train_error_std_3, label='train MSE')
#plt.errorbar(Ns/ntrain, test_error_mean_3, test_error_std_3, label='test MSE')
plt.plot(Ns/ntrain, train_error_mean_3, label='train MSE')
plt.plot(Ns/ntrain, test_error_mean_3, label='test MSE')
plt.plot(Ns/ntrain, weights_mean_3, label='weights')
#plt.ylim(0, 5)
plt.title('Third setting')
plt.ylabel('Error')
plt.xlabel('N/n')
plt.legend()
plt.show()

plt.plot(Ns/ntrain, test_error_mean_3, label='test MSE')
plt.plot(Ns/ntrain, weights_mean_3, label='weights')
plt.title('Third setting')
plt.ylabel('Error')
plt.yscale('log')
plt.xlabel('N/n')
plt.legend()
plt.show()




plt.plot(Ns/ntrain, test_error_mean_1, label='Setting 1')
plt.plot(Ns/ntrain, test_error_mean_2, label='Setting 2')
plt.plot(Ns/ntrain, test_error_mean_3, label='Setting 3')
#plt.ylim(0, 5)
plt.title('Test errors')
plt.ylabel('Error')
plt.xlabel('N/n')
plt.legend()
plt.show()

plt.plot(Ns/ntrain, test_error_mean_1, label='Setting 1')
plt.plot(Ns/ntrain, test_error_mean_2, label='Setting 2')
plt.plot(Ns/ntrain, test_error_mean_3, label='Setting 3')
plt.title('Test errors')
plt.ylabel('Error')
plt.yscale('log')
plt.xlabel('N/n')
plt.legend()
plt.show()


print("End")

