import tensorflow as tf
import numpy as np
import scipy
from sklearn.utils import shuffle

from sklearn.linear_model import Ridge
from sklearn.metrics import mean_squared_error
from matplotlib import pyplot as plt

gz = tf.keras.utils.get_file('HIGGS.csv.gz', 'http://mlphysics.ics.uci.edu/data/higgs/HIGGS.csv.gz')

d = 28
ntrain = 4000
ntest = 500
l = 1e-3

Ns = (np.arange(50) + 0.1) * ntrain / 10
T = 3
l = 1e-3

Ns = Ns.astype(np.int64)

print(train_images.shape)

def sample_spherical(npoints, ndim):
    vec = np.random.randn(npoints, ndim)
    vec = vec / np.linalg.norm(vec, axis=1)[:, np.newaxis]
    return vec * np.sqrt(ndim)

def sample_theta(outerdim, innerdim):
    theta = np.random.randn(outerdim, innerdim)
    theta = theta / np.linalg.norm(theta, axis=1)[:, np.newaxis]
    return theta * np.sqrt(innerdim)

def f1(x, noise=True, sigma=np.sqrt(0.5)):
    vec = x[:,0]
    if noise is True:
        vec = vec + sigma*np.random.randn(vec.size)
    return vec

def f2(x, noise=False, sigma=np.sqrt(0.5)):
    vec = x[:,0] + (x[:,0]**2 - 1)/2
    if noise is True:
        vec = vec + sigma*np.random.randn(vec.size)
    return vec

def ReLU(x):
    return x * (x > 0)

test_error_matrix = np.zeros((Ns.size, T))
train_error_matrix = np.zeros((Ns.size, T))

for i, N in enumerate(Ns):
    for t in np.arange(T):
        xtrain = train_images
        xtest = test_images

        ytrain = train_labels == 0
        ytest = test_labels == 0
        
        theta = sample_theta(N, d)
        print(xtrain.shape)
        print(theta.shape)
        Xtrain = xtrain @ theta.T / np.sqrt(d)
        Xtrain = ReLU(Xtrain)

        Xtest = xtest @ theta.T / np.sqrt(d)
        Xtest = ReLU(Xtest)

        model = Ridge(alpha = l*N/d, tol = 1e-3)
        model.fit(Xtrain, ytrain)

        ytest_pred = model.predict(Xtest)
        ytrain_pred = model.predict(Xtrain)

        train_error = mean_squared_error(ytrain, ytrain_pred)
        test_error = mean_squared_error(ytest, ytest_pred)

        train_error_matrix[i, t] = train_error
        test_error_matrix[i, t] = test_error

train_error_mean = np.mean(train_error_matrix, axis=1)
test_error_mean = np.mean(test_error_matrix, axis=1)
train_error_std = np.std(train_error_matrix, axis=1)
test_error_std = np.std(test_error_matrix, axis=1)

plt.errorbar(Ns/ntrain, train_error_mean, train_error_std, label='train MSE')
plt.errorbar(Ns/ntrain, test_error_mean, test_error_std, label='test MSE')
plt.legend()
plt.show()

print("End")



