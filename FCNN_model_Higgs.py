import tensorflow as tf
import numpy as np
from sklearn.utils import shuffle
from matplotlib import pyplot as plt

import tensorflow_docs.modeling
import tensorflow_docs.plots

gz = tf.keras.utils.get_file('HIGGS.csv.gz', 'http://mlphysics.ics.uci.edu/data/higgs/HIGGS.csv.gz')
ds = tf.data.experimental.CsvDataset(gz,[float(),]*(FEATURES+1), compression_type="GZIP")

def pack_row(*row):
  label = row[0]
  features = tf.stack(row[1:],1)
  return features, label

packed_ds = ds.batch(10000).map(pack_row).unbatch()


d = 28


for i, H in enumerate(hidden_dim):
    model = tf.keras.models.Sequential([
        tf.keras.layers.Flatten(input_shape=(28,28)),
        tf.keras.layers.Dense(H, activation = 'relu', kernel_regularizer=tf.keras.regularizers.l2(l)),
        tf.keras.layers.Dense(10, kernel_regularizer=tf.keras.regularizers.l2(l))
    ])

    model.compile(optimizer='adam',
                  loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics = ['accuracy']
    )

    model.summary()
    trainableParams = np.sum([np.prod(v.get_shape()) for v in model.trainable_weights])
    params[i] = trainableParams

    model.fit(train_images, train_labels, epochs=50)

    train_loss, train_acc = model.evaluate(train_images, train_labels, verbose = 2)
    test_loss, test_acc = model.evaluate(test_images, test_labels, verbose = 2)

    train_error[i] = train_loss
    test_error00[i] = test_loss

    train_accuracy[i] = train_acc
    test_accuracy[i] = test_acc


plt.plot(params / ntrain, train_error, label = 'train CE')
plt.plot(params / ntrain, test_error, label = 'test CE')
plt.legend()
plt.show()

plt.plot(params / ntrain, 1 - train_accuracy, label = 'train err')
plt.plot(params / ntrain, 1 - test_accuracy, label = 'test err')
plt.legend()
plt.show()
