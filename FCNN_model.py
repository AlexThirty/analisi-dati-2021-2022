import os
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
import tensorflow as tf
import numpy as np
from sklearn.utils import shuffle
from sklearn.preprocessing import OneHotEncoder
from matplotlib import pyplot as plt

(train_images, train_labels), (test_images, test_labels) = tf.keras.datasets.mnist.load_data()

train_images = train_images.reshape(train_images.shape[0], 28, 28, 1).astype('float32')
test_images = test_images.reshape(test_images.shape[0], 28, 28, 1).astype('float32')

train_images = train_images / 255.0
test_images = test_images / 255.0

ntrain = 4000
ntest = 1000
l = 1
T = 1

#digit = 0
#train_labels = train_labels == digit
#test_labels = test_labels == digit


#incorrect_pos = np.random.binomial(1, 0.15, train_images.shape[0])
#incorrect_digit = np.random.randint(0, 10, train_images.shape[0])
#train_labels = train_labels * (1 - incorrect_pos) + incorrect_pos * incorrect_digit


one_enc = OneHotEncoder(sparse=False)
train_labels = train_labels.reshape((train_labels.size, 1))
test_labels = test_labels.reshape((test_labels.size, 1))
one_enc.fit(train_labels)
train_labels = one_enc.transform(train_labels)
test_labels = one_enc.transform(test_labels)

print(train_labels.shape)
print(test_labels.shape)

print(train_images.shape)
print(test_images.shape)

hidden_dim = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25, 30, 35, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 60, 70, 80, 100])
params = np.zeros(hidden_dim.shape)

train_error = np.zeros((hidden_dim.size, T))
test_error = np.zeros((hidden_dim.size, T))

train_accuracy = np.zeros((hidden_dim.size, T))
test_accuracy = np.zeros((hidden_dim.size, T))

train_images, train_labels = shuffle(train_images, train_labels)
test_images, test_labels = shuffle(test_images, test_labels)
xtrain = train_images[0:ntrain, :, :]
ytrain = train_labels[0:ntrain, :]
xtest = test_images[0:ntest, :, :]
ytest = test_labels[0:ntest, :]

for t in range(T):

    for i, H in enumerate(hidden_dim):
        model = tf.keras.models.Sequential([
            tf.keras.layers.Flatten(input_shape=(28,28)),
            tf.keras.layers.Dense(H, activation = 'relu'),
            tf.keras.layers.Dense(10)
        ])

        model.compile(optimizer='adam',
                        loss = tf.keras.losses.MeanSquaredError(),
                        metrics = ['accuracy']
        )

        model.summary()
        trainableParams = np.sum([np.prod(v.get_shape()) for v in model.trainable_weights])
        params[i] = trainableParams
    
        model.fit(xtrain, ytrain, epochs=600)

        train_loss, train_acc = model.evaluate(xtrain, ytrain, verbose = 2)
        test_loss, test_acc = model.evaluate(xtest, ytest, verbose = 2)

        train_error[i, t] = train_loss
        test_error[i, t] = test_loss
        print(train_loss)
        print(test_loss)
        train_accuracy[i, t] = train_acc
        test_accuracy[i, t] = test_acc

train_error_mean = np.mean(train_error, axis=1)
test_error_mean = np.mean(test_error, axis=1)
train_accuracy_mean = np.mean(train_accuracy, axis=1)
test_accuracy_mean = np.mean(test_accuracy, axis=1)

plt.plot(params / ntrain, train_error_mean, label = 'train MSE')
plt.plot(params / ntrain, test_error_mean, label = 'test MSE')
plt.xlabel('params/n')
plt.ylabel('MSError')
plt.legend()
plt.show()

print(train_error_mean)
print(test_error_mean)

plt.plot(params / ntrain, 1 - train_accuracy_mean, label = 'train err')
plt.plot(params / ntrain, 1 - test_accuracy_mean, label = 'test err')
plt.xlabel('params/n')
plt.ylabel('Class error')
plt.legend()
plt.show()