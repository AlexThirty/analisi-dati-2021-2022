import os
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
import tensorflow as tf
import numpy as np
import scipy
from sklearn.linear_model import Ridge
from sklearn.metrics import mean_squared_error
from matplotlib import pyplot as plt

d = 100
ntrain = 4000
ntest = 1000
Ns = np.array([2, 4, 6, 8, 10, 15, 20, 25, 30, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 50, 55, 60, 70, 80, 100])
T = 5
l = 1e-3

Ns = Ns.astype(np.int64)

def sample_spherical(npoints, ndim):
    vec = np.random.randn(npoints, ndim)
    vec = vec / np.linalg.norm(vec, axis=1)[:, np.newaxis]
    return vec * np.sqrt(ndim)

def sample_theta(outerdim, innerdim):
    theta = np.random.randn(outerdim, innerdim)
    theta = theta / np.linalg.norm(theta, axis=1)[:, np.newaxis]
    return theta * np.sqrt(innerdim)

def f1(x, noise=True, sigma=np.sqrt(0.5)):
    vec = x[:,0]
    if noise is True:
        vec = vec + sigma*np.random.randn(vec.size)
    return vec

def f2(x, noise=False, sigma=np.sqrt(0.5)):
    vec = x[:,0] + (x[:,0]**2 - 1)/2
    if noise is True:
        vec = vec + sigma*np.random.randn(vec.size)
    return vec

def ReLU(x):
    return x * (x > 0)

test_error_matrix = np.zeros((Ns.size, T))
train_error_matrix = np.zeros((Ns.size, T))

for t in np.arange(T):
    xtrain = sample_spherical(ntrain, d)
    xtest = sample_spherical(ntest, d)
    ytrain = f2(xtrain)
    ytest = f2(xtest)
    params = np.zeros(Ns.shape)
    for i, N in enumerate(Ns):
        model = tf.keras.models.Sequential([
            tf.keras.layers.InputLayer(input_shape=d),
            tf.keras.layers.Dense(N, activation = 'relu'),
            tf.keras.layers.Dense(1)
        ])

        model.compile(optimizer='adam',
                        loss = tf.keras.losses.MeanSquaredError(),
                        metrics = [tf.keras.metrics.MeanSquaredError()]
        )

        model.summary()
        trainableParams = np.sum([np.prod(v.get_shape()) for v in model.trainable_weights])
        params[i] = trainableParams
    
        model.fit(xtrain, ytrain, epochs=100)

        train_loss, _ = model.evaluate(xtrain, ytrain, verbose = 2)
        test_loss, _ = model.evaluate(xtest, ytest, verbose = 2)

        train_error_matrix[i, t] = train_loss
        test_error_matrix[i, t] = test_loss

train_error_mean = np.mean(train_error_matrix, axis=1)
test_error_mean = np.mean(test_error_matrix, axis=1)
train_error_std = np.std(train_error_matrix, axis=1)
test_error_std = np.std(test_error_matrix, axis=1)

plt.plot(params / ntrain, train_error_mean, label='train MSE')
plt.plot(params / ntrain, test_error_mean, label='test MSE')
plt.xlabel('N/n')
plt.ylabel('Error')
plt.title('FCNN random model')
plt.legend()
plt.show()

print("End")